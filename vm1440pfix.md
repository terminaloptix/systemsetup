Fix qemu resolution for 1440p VMs
```
xrandr --newmode "2560x1440_74.97" 296 2560 2568 2600 2666 1440 1443 1448 1481 +HSync -VSync
```
```
sudo nano /etc/X11/xorg.conf.d/10-monitor.conf
```
```
Section "Monitor"
    Identifier "Virtual-1"
    Modeline "2560x1440_74.97" 296 2560 2568 2600 2666 1440 1443 1448 1481 +HSync -VSync    
    Option "PreferredMode" "2560x1440_74.97"
EndSection
Section "Screen"
    Identifier "Screen0"
    Monitor "Virtual-1"
    DefaultDepth 24
    SubSection "Display"
    Modes "2560x1440_74.97"
    EndSubSection
EndSection
```