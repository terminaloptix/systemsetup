Setup fstab
```
192.x.x.2:/xx /home/xx/xx nfs noauto,_netdev,nofail,noatime,hard,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10 0 0
192.x.x.2:/xx /home/xx/xx nfs noauto,_netdev,nofail,noatime,hard,x-systemd.automount,x-systemd.requires=network-online.target,x-systemd.device-timeout=10 0 0
```
Install base xfce packages
```
sudo pacman -S sof-firmware xiccd neofetch neovim xclip ufw virt-viewer gufw nfs-utils engrampa gimp inkscape libreoffice-fresh zsh lsd thunderbird git viewnior gtk-engine-murrine lightdm-gtk-greeter-settings base-devel dnsutils traceroute vlc whois materia-gtk-theme papirus-icon-theme noto-fonts ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid ttf-ibm-plex ttf-liberation ttf-caladea ttf-carlito ttf-opensans ttf-roboto ttf-ubuntu-font-family tex-gyre-fonts otf-overpass
```
Remove unused xfce packages
```
sudo pacman -Rns xfburn xfce4-clipman-plugin xfce4-notes-plugin ristretto parole
```
Setup ZSH
```
chsh -s /bin/zsh
```
```
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
```
```
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
```
```
git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
```
Setup firewall
```
sudo gufw
```
```
sudo systemctl enable ufw
```
```
sudo systemctl start ufw
```
Install nvchad
```
git clone https://github.com/NvChad/starter ~/.config/nvim && nvim
```
```
:MasonInstallAll
```
Setup xcursor-simplesoft
```
copy /simpleandsoft/ to /usr/share/icons/
```
```
in /usr/share/icons/default/index.theme set Simple-and-Soft as default
```