Cleanup arch linux
-
Check systemd failed services
```
systemctl --failed
```
Log files check
```
sudo journalctl -p 3 -xb
```
Update
```
sudo pacman -Syu
```
Yay Update
```
yay
```
Delete Pacman Cache
```
sudo pacman -Scc
```
Delete Yay Cache
```
yay -Scc
```
Delete unwanted dependencies
```
yay -Yc
```
Check Orphan packages
```
pacman -Qtdq
```
Remove Orphan packages
```
sudo pacman -Rns $(pacman -Qtdq)
```
Clean the Cache
```
rm -rf .cache/*
```
Clean the journal
```
sudo journalctl --vacuum-time=2weeks
```
For lightdm to set display before x loads .xprofile
```
xrandr --output HDMI-1 --primary --mode 2560x1440 --rate 75 --dpi 96 --pos 0x0 --rotate normal --output eDP-1 --off
```